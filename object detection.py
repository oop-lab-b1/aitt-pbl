import numpy as np
import tensorflow as tf
model =tf.keras.models.load_model('ssd_mobilenet_v2_coco_2018_03_29/saved_model’)
colors = np.random.uniform(0, 255, size=(len(classes), 3))
img = cv2.imread('/content/download (1).jpg')
img = cv2.resize(img, (300, 300))
img = img.astype('float32') / 255
img = np.expand_dims(img, axis=0)
detections = model.predict(img)
for i in range(detections.shape[1]):
  class_id = int(detections[0, i, 1])
  score = detections[0, i, 2]
  if score > 0.5:
    bbox = detections[0, i, 3:7] * np.array([img.shape[2], img.shape[1],img.shape[2], img.shape[1]])
  x, y, w, h = bbox.astype('int')
  label = '{}: {:.2f}'.format(classes[class_id], score)
  color = colors[class_id]
  cv2.rectangle(img, (x, y), (w, h), color, 2)
  cv2.putText(img, label, (x, y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5,color, 2)
  cv2.imshow('Object Detection', img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()
